# API 용어 설명

![API](images/api.png)

## 1. Path

## 2. Operation

## 3. Schema

## 4. Request

## 5. Reponse

---

# Path

> - Path는 정의된 API의 Endpoint 입니다. 기본 Host URL뒤에 적용되며 {Param}를 이용하여 URL Parameter를 사용할 수 있습니다.
> - Path에는 POST, GET, PUT, DELETE 4가지의 작업을 지정할 수 있습니다.
> - ex) GET www.example.com/api/user

---

# Operation

> 생성된 PATH에 POST, GET, PUT, DELETE의 Operation을 생성하고 정의할 수 있습니다.

---

# Schema

> Schema는 다른 구성요소(Request, Response)등에서 사용 할 수 있는 데이터 유형을 나타냅니다.
> Schema는 아래와 같은 데이터 유형을 사용 할 수 있습니다.

> ## Schema 데이터 유형
>
> | 이름    | 유형                                          |
> | ------- | --------------------------------------------- |
> | String  | 문자열                                        |
> | Boolean | "true" or "False"                             |
> | Number  | 숫자형 데이터 소숫점 사용 가능                |
> | Integer | 정수형 데이터                                 |
> | Object  | 다양한 데이터 유형을 포함한 객체형            |
> | Array   | 기존의 존재하는 Schema의 배열형               |
> | OneOf   | 다양한 Schema중에서 한개를 선택할수 있는 유형 |

---

# Request

> Request는 POST, PUT에 사용되는 Operation들에 Input값으로 사용되며 각 Operation마다 한개의 Request를 사용 할 수 있습니다.

---

# Response

> Response는 각 Operation에서 Output값으로 정의되며 각 Operation마다 성공 및 에러상황에 최소 한개 이상의 Response를 사용 할 수 있습니다.
